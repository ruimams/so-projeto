#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <semaphore.h>
#include <kos_client.h>
#include <buffer.h>

BUF_t* buffer;
int buffer_size;
pthread_mutex_t buffer_mutex;

void buffer_init(int buf_size) {
	int i;

	buffer = (BUF_t*) malloc(sizeof(BUF_t) * buf_size);
	buffer_size = buf_size;

	for (i = 0; i < buf_size; i++) {
		buffer[i].index = i;
		buffer[i].status = 0;
		buffer[i].fn = 0;
	}
}

BUF_t buffer_get_task() {
	int i;

	pthread_mutex_lock(&buffer_mutex);

	for (i = 0; i < buffer_size; i++) {
		if (buffer[i].status == 1)
			break;
	}
	
	buffer[i].status = 2;
	pthread_mutex_unlock(&buffer_mutex);
	
	return buffer[i];
}

void buffer_put(int fn, int shardId, char* key, char* value, char** r_value, int* r_dim, KV_t** r_array, sem_t *client) {
	int i;
	
	pthread_mutex_lock(&buffer_mutex);
	
	for (i = 0; i < buffer_size; i++) {
		if (buffer[i].status == 0)
			break;
	}
	buffer[i].index = i;
	buffer[i].status = 1;
	buffer[i].fn = fn;
	buffer[i].shardId = shardId;
	buffer[i].key = strndup(key, KV_SIZE);
	buffer[i].value = strndup(value, KV_SIZE);
	buffer[i].r_value = r_value;
	buffer[i].r_dim = r_dim;
	buffer[i].r_array = r_array;
	buffer[i].client = client;

	pthread_mutex_unlock(&buffer_mutex);
}

void buffer_put_get(int shardId, char* key, char** r_value, sem_t *client) {
	buffer_put(1, shardId, key, "", r_value, NULL, NULL, client);
}

void buffer_put_put(int shardId, char* key, char* value, char** r_value, sem_t *client) {
	buffer_put(2, shardId, key, value, r_value, NULL, NULL, client);
}

void buffer_put_remove(int shardId, char* key, char** r_value, sem_t *client) {
	buffer_put(3, shardId, key, "", r_value, NULL, NULL, client);
}

void buffer_put_getAllKeys(int shardId, int* r_dim, KV_t** r_array, sem_t *client) {
	buffer_put(4, shardId, "", "", NULL, r_dim, r_array, client);
}

void buffer_set_status(int index, int status) {
	pthread_mutex_lock(&buffer_mutex);
	buffer[index].status = status;
	pthread_mutex_unlock(&buffer_mutex);
}
