#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <semaphore.h>
#include <string.h>
#include <kos_file.h>
#include <hash.h>
#include <hashtable_table.h>


FILE **files;
int *nkv, *biggest_free_slot;
char *input_format;
sem_t *sem_files;
Y **free_slots;


void file_init(int nShards) {
	int i;
	char *filename;

	files = (FILE **) malloc(sizeof(FILE *) * nShards);
	nkv = (int *) malloc(sizeof(int) * nShards);
	sem_files = (sem_t *) malloc(sizeof(sem_t) * nShards);
	free_slots = (Y **) malloc(sizeof(Y *) * nShards);
	biggest_free_slot = (int *) malloc(sizeof(int) * nShards);

	input_format = (char *) malloc(sizeof(char) * (17 + (int_len(KV_SIZE-1) * 2) ) );
	sprintf(input_format, "%%*[ <]%%%d[^,],%%%d[^>]>", KV_SIZE-1, KV_SIZE-1);

	filename = (char *) malloc(sizeof(char) * 7 + int_len(nShards));

	for (i = 0; i < nShards; i++) {
		sprintf(filename, "fshard%d", i);

		if (access(filename, F_OK) != -1) {
			files[i] = fopen(filename, "r+");
			nkv[i] = file_get_nkv(i);
		}
		else {
			files[i] = fopen(filename, "w+");
			nkv[i] = 0;
			fputc('0', files[i]);
			fputc(' ', files[i]);
			fflush(files[i]);
		}

		sem_init(&sem_files[i], 0, 1);
		free_slots[i] = (Y *) malloc(sizeof(Y));
		biggest_free_slot[i] = -1;
	}
}

void file_get_shard(int shardId) {
	int i;
	lst_iitem_t *new;

	nkv[shardId] = file_get_nkv(shardId);

	for (i = 0; i < nkv[shardId]; i++) {
		new = (lst_iitem_t*) malloc(sizeof(lst_iitem_t));

		new->pos = ftell(files[shardId]);
		fscanf(files[shardId], input_format, new->kv.key, new->kv.value);
		
		new->next = hashtable[shardId][hash(new->kv.key)]->first;
		
		hashtable[shardId][hash(new->kv.key)]->first = new;
	}
}

void file_new_key(int shardId, lst_iitem_t *item) {
	sem_wait(&sem_files[shardId]);

	file_new_key_write(shardId, item);

	file_increment_nkv(shardId);

	fflush(files[shardId]);

	sem_post(&sem_files[shardId]);
}

void file_set_key(int shardId, lst_iitem_t *item, const char* old_value) {
	int space_left = strlen(old_value) - strlen(item->kv.value);

	sem_wait(&sem_files[shardId]);
	
	if (space_left >= 0) {
		fseek(files[shardId], item->pos, SEEK_SET);

		fprintf(files[shardId], "<%s,%s>", item->kv.key, item->kv.value);

		file_free_space(shardId, space_left);

		fflush(files[shardId]);
	}
	else {
		fseek(files[shardId], item->pos, SEEK_SET);

		file_free_space(shardId, 3 + strlen(item->kv.key) + strlen(old_value));
		
		file_new_key_write(shardId, item);

		fflush(files[shardId]);
	}

	sem_post(&sem_files[shardId]);
}

void file_remove_key(int shardId, lst_iitem_t *item) {
	sem_wait(&sem_files[shardId]);

	fseek(files[shardId], item->pos, SEEK_SET);

	file_free_space(shardId, 3 + strlen(item->kv.key) + strlen(item->kv.value));

	file_decrement_nkv(shardId);

	fflush(files[shardId]);

	sem_post(&sem_files[shardId]);
}

void file_increment_nkv(int shardId) {
	int spare_space;
	char c;

	nkv[shardId] = file_get_nkv(shardId) + 1;

	for (spare_space = 0; (c = fgetc(files[shardId])) == ' '; spare_space++) {}

	if (int_len(nkv[shardId]) == int_len(nkv[shardId]-1) || spare_space > 0) {
		rewind(files[shardId]);

		fprintf(files[shardId], "%d", nkv[shardId]);
	}
	else {
		file_compact(shardId);
	}
}

void file_decrement_nkv(int shardId) {
	nkv[shardId] = file_get_nkv(shardId) - 1;
	
	rewind(files[shardId]);

	fprintf(files[shardId], "%d", nkv[shardId]);

	if (int_len(nkv[shardId]) < int_len(nkv[shardId]+1)) {
		fputc(' ', files[shardId]);
	}
}

void file_compact(int shardId) {
	int i;
	lst_iitem_t *curr;
	X *curr_fs, *next_fs;

	rewind(files[shardId]);

	fprintf(files[shardId], "%d ", nkv[shardId]);

	for (i = 0; i < HT_SIZE; i++) {
		curr = hashtable[shardId][i]->first;
		
		while (curr != NULL) {
			curr->pos = ftell(files[shardId]);
			fprintf(files[shardId], "<%s,%s>", curr->kv.key, curr->kv.value);

			curr = curr->next;
		}
	}

	ftruncate(fileno(files[shardId]), ftello(files[shardId]));


	biggest_free_slot[shardId] = -1;

	curr_fs = free_slots[shardId]->first;
	while (curr_fs != NULL) {
		next_fs = curr_fs->next;
		free(curr_fs);
		curr_fs = next_fs;
	}

	free_slots[shardId]->first = NULL;
}

int file_get_nkv(int shardId) {
	int n;

	rewind(files[shardId]);

	fscanf(files[shardId], "%d", &n);

	return n;
}

void file_erase(FILE *file, int size) {
	int i;

	for (i = 0; i < size; i++) {
		fputc(' ', file);
	}
}

void file_new_key_write(int shardId, lst_iitem_t *item) {
	int pair_size, free_space, remaining_space;
	
	pair_size = strlen(item->kv.key) + strlen(item->kv.value) + 3;
	free_space = file_get_free_slot(shardId, pair_size);
	remaining_space = free_space - pair_size;

	item->pos = ftell(files[shardId]);
	fprintf(files[shardId], "<%s,%s>", item->kv.key, item->kv.value);

	if (remaining_space > 0) {
		file_set_free_slot(shardId, ftell(files[shardId]), remaining_space);
	}
}

int file_get_free_slot(int shardId, int size) {
	int free_space;
	X *curr, *prev;

	if (biggest_free_slot[shardId] >= size) {
		prev = NULL;
		curr = free_slots[shardId]->first;
			
		while (curr != NULL) {
			if (curr->space >= size) {
				fseek(files[shardId], curr->start, SEEK_SET);
				free_space = curr->space;

				if (prev == NULL) {
					free_slots[shardId]->first = curr->next;
				}
				else {
					prev->next = curr->next;
				}
				
				free(curr);
				return free_space;
			}
			
			prev = curr;
			curr = curr->next;
		}
	}

	fseek(files[shardId], 0L, SEEK_END);

	return size;
}

void file_set_free_slot(int shardId, long start, int space) {
	X *new;

	new = (X *) malloc(sizeof(X));

	new->start = start;
	new->space = space;
	new->next = free_slots[shardId]->first;
	
	free_slots[shardId]->first = new;

	if (space > biggest_free_slot[shardId]) {
		biggest_free_slot[shardId] = space;
	}
}

void file_free_space(int shardId, int space) {
	char c;
	X *curr, *inception_curr, *inception_prev;
	long start, new_end;
	int before = FALSE, after = FALSE;

	start = ftell(files[shardId]);

	file_erase(files[shardId], space);
	
	// if there is a "free slot" next to the one we're creating join them
	if ((c=fgetc(files[shardId])) == ' ') {
		after = TRUE; // there is a free slot right after the one we're creating
	}

	fseek(files[shardId], start-1, SEEK_SET);
	if ((c=fgetc(files[shardId])) == ' ') {
		before = TRUE; // there is a free slot right before the one we're creating
	}

	if (before || after) {
		curr = free_slots[shardId]->first;
		
		while (curr != NULL) {
			if (before) {
				if (curr->start + curr->space == start) {
					curr->space = curr->space + space;

					if (after) {
						new_end = curr->start + curr->space;
						inception_curr = free_slots[shardId]->first;
						inception_prev = NULL;

						while (inception_curr != NULL) {
							if (inception_curr != curr)	{ // no point in checking the same
								if (inception_curr->start == new_end) {
									curr->space = curr->space + inception_curr->space;

									if (inception_prev == NULL)
										free_slots[shardId]->first = inception_curr->next;
									else
										inception_prev->next = inception_curr->next;

									free(inception_curr);
									break;
								}
							}

							inception_prev = inception_curr;
							inception_curr = inception_curr->next;
						}
					}

					break;
				}
			}

			if (after) {
				if (start+space == curr->start) {
					curr->start = start;
					curr->space = curr->space + space;

					if (before) {
						inception_curr = free_slots[shardId]->first;
						inception_prev = NULL;

						while (inception_curr != NULL) {
							if (inception_curr != curr)	{ // no point in checking the same
								if ((inception_curr->start + inception_curr->space) == start) {
									curr->start = inception_curr->start;
									curr->space = curr->space + inception_curr->space;

									if (inception_prev == NULL)
										free_slots[shardId]->first = inception_curr->next;
									else
										inception_prev->next = inception_curr->next;

									free(inception_curr);
									break;
								}
							}

							inception_prev = inception_curr;
							inception_curr = inception_curr->next;
						}
					}

					break;
				}
			}

			curr = curr->next;
		}

		if (curr != NULL) {
			if (curr->space > biggest_free_slot[shardId]) {
				biggest_free_slot[shardId] = curr->space;
			}
		}
		else { // only "free slot" in this file
			file_set_free_slot(shardId, start, space);
		}
	}
	else {
		file_set_free_slot(shardId, start, space);
	}
}


int int_len(int value) {
	int l = 1;

	while (value > 9) {
		l++;
		value /= 10;
	}

	return l;
}
