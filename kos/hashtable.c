#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <semaphore.h>
#include <kos_client.h>
#include <hashtable.h>
#include <hash.h>
#include <kos_file.h>
#include <hashtable_table.h>
#include <delay.h>

sem_t **sem_readers, **sem_writers;
pthread_mutex_t **mutex;
int **nReaders, **writing, **readers_waiting, **writers_waiting;
int temp_w = 0, temp_r = 0;


void hashtable_init(int num_shards) {
	int i, j;

	hashtable = (list_t***) malloc(sizeof(list_t**) * num_shards);

	sem_readers = (sem_t**) malloc(sizeof(sem_t*) * num_shards);
	sem_writers = (sem_t**) malloc(sizeof(sem_t*) * num_shards);

	mutex = (pthread_mutex_t**) malloc(sizeof(pthread_mutex_t*) * num_shards);

	nReaders = (int**) malloc(sizeof(int*) * num_shards);
	writing = (int**) malloc(sizeof(int*) * num_shards);
	readers_waiting = (int**) malloc(sizeof(int*) * num_shards);
	writers_waiting = (int**) malloc(sizeof(int*) * num_shards);

	file_init(num_shards);
	
	for (i = 0; i < num_shards; i++) {
		hashtable[i] = (list_t**) malloc(sizeof(list_t*) * HT_SIZE);

		sem_readers[i] = (sem_t*) malloc(sizeof(sem_t) * HT_SIZE);
		sem_writers[i] = (sem_t*) malloc(sizeof(sem_t) * HT_SIZE);

		mutex[i] = (pthread_mutex_t*) malloc(sizeof(pthread_mutex_t) * HT_SIZE);

		nReaders[i] = (int*) malloc(sizeof(int) * HT_SIZE);
		writing[i] = (int*) malloc(sizeof(int) * HT_SIZE);
		readers_waiting[i] = (int*) malloc(sizeof(int) * HT_SIZE);
		writers_waiting[i] = (int*) malloc(sizeof(int) * HT_SIZE);

		for (j = 0; j < HT_SIZE; j++) {
			hashtable[i][j] = (list_t*) malloc(sizeof(list_t));
			hashtable[i][j]->first = NULL;


			sem_init(&sem_readers[i][j], 0, 0);
			sem_init(&sem_writers[i][j], 0, 0);

			nReaders[i][j] = 0;
			writing[i][j] = FALSE;
			readers_waiting[i][j] = 0;
			writers_waiting[i][j] = 0;
		}

		file_get_shard(i);
	}
}


char* hashtable_get(int shardId, char* key) {
	lst_iitem_t *curr;
	int list = hash(key);

	start_read(shardId, list);
	
	curr = hashtable[shardId][list]->first;
	
	while (curr != NULL) {
		
		if (!(strcmp(curr->kv.key, key))) {
			end_read(shardId, list);
			return strndup(curr->kv.value, KV_SIZE);
		}
		
		curr = curr->next;
	}

	end_read(shardId, list);

	return NULL;
}


char* hashtable_put(int shardId, char* key, char* value) {
	lst_iitem_t *curr, *new;
	char *old_value = (char *) malloc(sizeof(char) * KV_SIZE);
	int list = hash(key);

	start_read(shardId, list);

	curr = hashtable[shardId][list]->first;
	
	while (curr != NULL) {
		if (!(strcmp(curr->kv.key, key))) {
			strcpy(old_value, curr->kv.value);

			end_read_start_write(shardId, list);

			strcpy(curr->kv.value, value);

			end_write(shardId, list);

			file_set_key(shardId, curr, old_value);

			return old_value;
		}
		curr = curr->next;
	}
	
	end_read(shardId, list);

	new = (lst_iitem_t*) malloc(sizeof(lst_iitem_t));

	strncpy(new->kv.key, key, KV_SIZE);
	strncpy(new->kv.value, value, KV_SIZE);

	start_write(shardId, list);

	new->next = hashtable[shardId][list]->first;
	hashtable[shardId][list]->first = new;

	end_write(shardId, list);

	file_new_key(shardId, new);

	return NULL;
}


char* hashtable_remove(int shardId, char* key) {
	lst_iitem_t *curr, *previous;
	char *value = (char *) malloc(sizeof(char) * KV_SIZE);

	previous = NULL;
	int list = hash(key);

	start_read(shardId, list);

	curr = hashtable[shardId][list]->first;

	while (curr != NULL) {
		if (!(strcmp(curr->kv.key, key))) {
			strncpy(value, curr->kv.value, KV_SIZE);

			end_read_start_write(shardId, list);

			if (previous != NULL)
				previous->next = curr->next;
			else
				hashtable[shardId][list]->first = curr->next;

			end_write(shardId, list);

			file_remove_key(shardId, curr);

			free(curr);
			return value;
		}

		previous = curr;
		curr = curr->next;
	}

	end_read(shardId, list);

	return NULL;
}


KV_t* hashtable_getAllKeys(int shardId, int* dim) {
	KV_t *array;
	lst_iitem_t *curr;
	int size, i;

	array = (KV_t *) malloc(0);
	size = 0;

	for (i = 0; i < HT_SIZE; i++) {
		start_read(shardId, i);

		curr = hashtable[shardId][i]->first;
		
		while (curr != NULL) {
			array = (KV_t *) realloc(array, (sizeof(KV_t) * ++size));
			memcpy(&array[size - 1], &curr->kv, sizeof(KV_t));

			curr = curr->next;
		}

		end_read(shardId, i);
	}

	if (size == 0) {
		*dim = -1;
		return NULL;
	}

	*dim = size;
	return array;
}



void start_read(int s, int l) {
	pthread_mutex_lock(&mutex[s][l]);

	if (writing[s][l] || writers_waiting[s][l] > 0)	{
		readers_waiting[s][l]++;
		pthread_mutex_unlock(&mutex[s][l]);

		sem_wait(&sem_readers[s][l]);

		pthread_mutex_lock(&mutex[s][l]);
		if (readers_waiting[s][l] > 0) {
			nReaders[s][l]++;
			readers_waiting[s][l]--;
			sem_post(&sem_readers[s][l]);
		}
	}
	else {
		nReaders[s][l]++;
	}
	
	pthread_mutex_unlock(&mutex[s][l]);

	delay();
}

void end_read(int s, int l) {
	pthread_mutex_lock(&mutex[s][l]);

	nReaders[s][l]--;
	if (nReaders[s][l] == 0 && writers_waiting[s][l] > 0) {
		writing[s][l] = TRUE;
		writers_waiting[s][l]--;
		sem_post(&sem_writers[s][l]);
	}

	pthread_mutex_unlock(&mutex[s][l]);

	delay();
}

void start_write(int s, int l) {
	pthread_mutex_lock(&mutex[s][l]);

	if (writing[s][l] == TRUE || nReaders[s][l] > 0 || readers_waiting[s][l] > 0) {
		writers_waiting[s][l]++;
		pthread_mutex_unlock(&mutex[s][l]);

		sem_wait(&sem_writers[s][l]);

		pthread_mutex_lock(&mutex[s][l]);
	}
	writing[s][l] = TRUE;

	pthread_mutex_unlock(&mutex[s][l]);
}

void end_write(int s, int l) {
	pthread_mutex_lock(&mutex[s][l]);

	writing[s][l] = FALSE;
	if (readers_waiting[s][l] > 0) {
		nReaders[s][l]++;
		readers_waiting[s][l]--;
		sem_post(&sem_readers[s][l]);
	}
	else if (writers_waiting[s][l] > 0) {
		writing[s][l] = TRUE;
		writers_waiting[s][l]--;
		sem_post(&sem_writers[s][l]);
	}

	pthread_mutex_unlock(&mutex[s][l]);
}

void end_read_start_write(int s, int l) {
	pthread_mutex_lock(&mutex[s][l]);

	nReaders[s][l]--;
	if (nReaders[s][l] == 0 && writers_waiting[s][l] > 0) {
		writing[s][l] = TRUE;
		writers_waiting[s][l]--;
		sem_post(&sem_writers[s][l]);
	}

	if (writing[s][l] == TRUE || nReaders[s][l] > 0 || readers_waiting[s][l] > 0) {
		writers_waiting[s][l]++;
		pthread_mutex_unlock(&mutex[s][l]);

		sem_wait(&sem_writers[s][l]);

		pthread_mutex_lock(&mutex[s][l]);
	}
	writing[s][l] = TRUE;

	pthread_mutex_unlock(&mutex[s][l]);
}