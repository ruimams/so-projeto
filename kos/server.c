#include <stdlib.h>
#include <semaphore.h>
#include <kos_client.h>
#include <kos.h>
#include <buffer.h>
#include <hashtable.h>
#include <delay.h>

void *server_thread(void *arg) {
	BUF_t task;

	while (1) {
		sem_wait(&sem_tasks);
		task = buffer_get_task();
		
		delay();

		switch (task.fn) {
			case 1 :
				*(task.r_value) = hashtable_get(task.shardId, task.key);
				break;
			case 2 :
				*(task.r_value) = hashtable_put(task.shardId, task.key, task.value);
				break;
			case 3 :
				*(task.r_value) = hashtable_remove(task.shardId, task.key);
				break;
			case 4 :
				*(task.r_array) = hashtable_getAllKeys(task.shardId, task.r_dim);
				break;
			default :
				// erro
				break;
		}
		
		buffer_set_status(task.index, 0);
		sem_post(&sem_buffer);
		sem_post(task.client);
	}

	return NULL;
}
