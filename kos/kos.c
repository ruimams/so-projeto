#include <kos_client.h>
#include <stdlib.h>
#include <stdio.h>

#include <semaphore.h>
#include <pthread.h>
#include <string.h>
#include <kos.h>
#include <buffer.h>
#include <server.h>
#include <hashtable.h> // necessario para 'hashtable_init()'

int shards_size, buffer_size;


int kos_init(int num_server_threads, int buf_size, int num_shards) {
	
	int i, s;
	pthread_t* server_threads = (pthread_t*) malloc(sizeof(pthread_t) * num_server_threads);

	buffer_size = buf_size;
	shards_size = num_shards;
	
	buffer_init(buf_size);
	
	hashtable_init(num_shards);
	
	sem_init(&sem_buffer, 0, buf_size);
	sem_init(&sem_tasks, 0, 0);

	for (i = 0; i < num_server_threads; i++) {
		if ((s = pthread_create(&server_threads[i], NULL, &server_thread, NULL))) {
			printf("pthread_create failed with code %d!\n", s);
			return -1;
		}
	}
	
	return 0;
}




char* kos_get(int clientid, int shardId, char* key) {

	char *r_value;
	sem_t this_client;

	if (!(shardId >= 0 && shardId < shards_size))
		return NULL;
	
	sem_init(&this_client, 0, 0);
	sem_wait(&sem_buffer);

	buffer_put_get(shardId, key, &r_value, &this_client);

	sem_post(&sem_tasks);
	sem_wait(&this_client);

	return r_value;
}




char* kos_put(int clientid, int shardId, char* key, char* value) {

	char *r_value;
	sem_t this_client;

	if (!(shardId >= 0 && shardId < shards_size))
		return NULL;
	
	sem_init(&this_client, 0, 0);
	sem_wait(&sem_buffer);

	buffer_put_put(shardId, key, value, &r_value, &this_client);
	
	sem_post(&sem_tasks);
	sem_wait(&this_client);
	
	return r_value; // previous value or NULL
}

char* kos_remove(int clientid, int shardId, char* key) {
	
	char *r_value;
	sem_t this_client;

	if (!(shardId >= 0 && shardId < shards_size))
		return NULL;

	sem_init(&this_client, 0, 0);
	sem_wait(&sem_buffer);

	buffer_put_remove(shardId, key, &r_value, &this_client);

	sem_post(&sem_tasks);
	sem_wait(&this_client);


	return r_value; // previous value or NULL
}

KV_t* kos_getAllKeys(int clientid, int shardId, int* dim) {
	
	int r_dim;
	KV_t *r_array;
	sem_t this_client;

	if (!(shardId >= 0 && shardId < shards_size)) {
		*dim = -1;
		return NULL;
	}

	sem_init(&this_client, 0, 0);
	sem_wait(&sem_buffer);
	
	buffer_put_getAllKeys(shardId, &r_dim, &r_array, &this_client);

	sem_post(&sem_tasks);
	sem_wait(&this_client);
	
	
	*dim = r_dim;
	return r_array;
}
