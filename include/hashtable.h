#include <stdio.h>
#include <kos_client.h>

#ifndef HAT_H
#define HAT_H 1

#define TRUE 1
#define FALSE 0

typedef struct lst_iitem {
	KV_t kv;
	long pos;
	struct lst_iitem *next;
} lst_iitem_t;

typedef struct {
	lst_iitem_t *first;
} list_t;


void hashtable_init(int num_shards);

char* hashtable_get(int shardId, char* key);

char* hashtable_put(int shardId, char* key, char* value);

char* hashtable_remove(int shardId, char* key);

KV_t* hashtable_getAllKeys(int shardId, int* dim);

void start_read(int s, int l);

void end_read(int s, int l);

void start_write(int s, int l);

void end_write(int s, int l);

void end_read_start_write(int s, int l);

#endif
