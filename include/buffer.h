#ifndef BUF_H
#define BUF_H 1

typedef struct {
	int index;
	int status; // 0 - free; 1 - task not assigned; 2 - task assigned
	int fn;
	int shardId;
	char* key;
	char* value;
	char** r_value;
	int* r_dim;
	KV_t** r_array;
	sem_t* client;
} BUF_t;


void buffer_init(int buf_size);

BUF_t buffer_get_task();

void buffer_put(int fn, int shardId, char* key, char* value, char** r_value, int* r_dim, KV_t** r_array, sem_t *client);

void buffer_put_get(int shardId, char* key, char** r_value, sem_t *client);

void buffer_put_put(int shardId, char* key, char* value, char** r_value, sem_t *client);

void buffer_put_remove(int shardId, char* key, char** r_value, sem_t *client);

void buffer_put_getAllKeys(int shardId, int* r_dim, KV_t** r_array, sem_t *client);

void buffer_set_status(int index, int status);

#endif
