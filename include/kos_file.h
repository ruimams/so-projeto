#include <stdio.h>
#include <hashtable.h>

#ifndef KOS_FILE_H
#define KOS_FILE_H 1

typedef struct Z {
	long start;
	int space;
	struct Z *next;
} X;

typedef struct {
	X *first;
} Y;

void file_init(int nShards);

void file_get_shard(int shardId);

void file_new_key(int shardId, lst_iitem_t *item);

void file_set_key(int shardId, lst_iitem_t *item, const char* new_value);

void file_remove_key(int shardId, lst_iitem_t *item);

void file_increment_nkv(int shardId);

void file_decrement_nkv(int shardId);

void file_compact(int shardId);

int file_get_nkv(int shardId);

void file_erase(FILE *file, int size);

void file_new_key_write(int shardId, lst_iitem_t *item);

int file_get_free_slot(int shardId, int size);

void file_set_free_slot(int shardId, long start, int space);

void file_free_space(int shardId, int space);

int int_len(int value);

#endif
